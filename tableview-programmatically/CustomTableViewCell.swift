//
//  CustomTableViewCell.swift
//  tableview-programmatically
//
//  Created by Ihwan ID on 29/08/20.
//  Copyright © 2020 Ihwan ID. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell{

    static let identifier = "CustomTableViewCell"

    private let _switch: UISwitch = {
        let _switch = UISwitch()
        _switch.onTintColor = .blue
        _switch.isOn = true
        return _switch
    }()

    private let myImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "gold")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    private let myLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .systemFont(ofSize: 17, weight: .bold)
        label.text = "hello world"
        return label
    }()


    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .orange
        contentView.addSubview(_switch)
        contentView.addSubview(myLabel)
        contentView.addSubview(myImageView)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func configure(text: String){
        myLabel.text = text
    }

    override func prepareForReuse() {
        myLabel.text = nil
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        let imageHeight = contentView.frame.size.height - 10
        let switchSize = _switch.sizeThatFits(contentView.frame.size)
        _switch.frame = CGRect(x: 5, y: (contentView.frame.size.height-switchSize.height)/2, width: switchSize.width, height: switchSize.height)

        myLabel.frame = CGRect(x: 10+_switch.frame.size.width, y: 0, width: contentView.frame.size.width - 10 - _switch.frame.size.width, height: contentView.frame.size.height)

        myImageView.frame = CGRect(x: contentView.frame.size.width-imageHeight-10, y: 5, width: imageHeight, height: imageHeight)
    }
    
}
